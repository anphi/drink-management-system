# Drink Management system

small django aplication to manage drinks and sales in households or shared flats.  
Tallies are no more!

### Features of the program:

* different user and guest accounts, all with their own balance
* possibility to transfer money between persons
* Possibility of prepaid payments and being in debt
* easy 2 click buying system
* history over the last sales
* touch friendly interface
* very few lines of JS

Please note that the pages aren't optimized for phones but rather for tablets.

### How to run

The easiest way to run it is via docker:
go to the `deployment` folder, and edit `docker-compose.yaml` to match your folders for the filesystem mappings and
run `docker-compose up` to start the application. Once it is up it should be available under `http://localhost:80`

### Usage

1. Start the application
2. Add prices for drinks you want to use
3. Add users to the system
4. Add your drinks. Make sure you use mostly squared images, otherwise they might look distorted
5. Your ready to go!

### Dependencies

* Python 3.8
* Python packages as listed in `requirements.py`
