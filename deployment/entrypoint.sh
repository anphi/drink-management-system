# collect static files
# and launch server

if [ ! -f drinkManagement/secrets.py ]; then
  echo "Please add an 'secrets.py' file to the drinkManagement directory"
  exit 1
fi

python3 manage.py migrate
python3 manage.py collectstatic --noinput
gunicorn drinkManagement.wsgi -b 0.0.0.0:8000
