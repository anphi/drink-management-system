from crispy_forms.bootstrap import InlineRadios
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div
from django.forms import ModelForm, Form, IntegerField, RadioSelect, HiddenInput

from .models import Product, Profile, Transaction, Price


class ChangePriceForm(ModelForm):
    class Meta:
        model = Product
        fields = ['current_price']

    def __init__(self, *args, **kwargs):
        super(ChangePriceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div('current_price')
        )


class DrinkForm(ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'current_price', 'image', 'active', 'dashboard_order', 'chart_color']

        help_texts = {
            'image': "Square images fit best. If your don't upload one, a white square is used instead."
        }

    def __init__(self, *args, **kwargs):
        super(DrinkForm, self).__init__(*args, **kwargs)
        self.fields['image'].required = False
        self.helper = FormHelper()
        self.helper.form_tag = False


class RestockForm(Form):
    bottles = IntegerField(min_value=0, required=True, initial=0)

    def __init__(self, *args, **kwargs):
        super(RestockForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div('bottles')
        )


class UserEditForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['username', 'active', 'sorting_order', 'room_number']
        widgets = {
            'sorting_order': RadioSelect()
        }

    def __init__(self, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div('username'),
            InlineRadios('sorting_order'),
            Div('active'),
            Div('room_number')
        )


class DebtForm(ModelForm):
    class Meta:
        model = Transaction
        fields = ['target', 'amount']

    def __init__(self, hide_source=False, *args, **kwargs):
        super(DebtForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div('target'),
            Div('amount'),
        )

        if hide_source:
            self.fields['target'].widget = HiddenInput()


class TransactionForm(ModelForm):
    class Meta:
        model = Transaction
        fields = ['target', 'source', 'amount']

    def __init__(self, *args, **kwargs):
        super(TransactionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div('source'),
            Div('target'),
            Div('amount'),
        )


class PriceForm(ModelForm):
    class Meta:
        model = Price
        fields = ['name', 'description', 'amount']

    def __init__(self, *args, **kwargs):
        super(PriceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div('name'),
            Div('description'),
            Div('amount'),
        )
