from colorfield.fields import ColorField
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models

# Create your models here.
from django.utils import timezone


class SingletonModel(models.Model):
    """Singleton Django Model"""

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        """
        Save object to the database. Removes all other entries if there
        are any.
        """
        self.__class__.objects.exclude(id=self.id).delete()
        super(SingletonModel, self).save(*args, **kwargs)

    @classmethod
    def load(cls):
        """
        Load object from the database. Failing that, create a new empty
        (default) instance of the object and return it (without saving it
        to the database).
        """

        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            return cls()


class ApplicationSettings(SingletonModel):
    pass


class Profile(models.Model):
    """
    simple profile for every user of the application
    """

    class ProfileType(models.IntegerChoices):
        FLATMATE = 0
        GUEST = 1

    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)

    username = models.CharField(max_length=50, blank=True)
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    email = models.EmailField(blank=True)
    active = models.BooleanField(help_text="Shows the user on the dashboard", default=True)
    sorting_order = models.PositiveSmallIntegerField(verbose_name="Role", choices=ProfileType.choices,
                                                     default=ProfileType.FLATMATE,
                                                     help_text="Accounts listed as guests will appear below flatmates")

    room_number = models.PositiveSmallIntegerField(null=True, blank=True)

    allowed_to_buy = models.BooleanField(default=True,
                                         help_text="Enable to lock the account from buying, for example when they owe "
                                                   "to much money")

    start_balance = models.FloatField(default=0, editable=False)

    balance = models.FloatField(default=0, verbose_name="Zwischengespeicherter Kontostand")

    @property
    def get_username(self):
        if self.user:
            return self.user.username
        else:
            return self.username

    def __str__(self):
        if self.user:
            return f"* {self.user.username}"
        else:
            return f"  {self.username}"

    def validate_balance(self):
        sum_of_outgoing_transactions = Transaction.objects.filter(source=self).aggregate(models.Sum("amount"))
        sum_of_incoming_transactions = Transaction.objects.filter(target=self).aggregate(models.Sum("amount"))
        sum_of_sales = Sale.objects.filter(profile=self).aggregate(models.Sum("price"))

        sum_of_outgoing_transactions = list(sum_of_outgoing_transactions.values())[0]
        sum_of_incoming_transactions = list(sum_of_incoming_transactions.values())[0]
        sum_of_sales = list(sum_of_sales.values())[0]
        if not sum_of_outgoing_transactions:
            sum_of_outgoing_transactions = 0
        if not sum_of_incoming_transactions:
            sum_of_incoming_transactions = 0
        if not sum_of_sales:
            sum_of_sales = 0
        self.balance = self.start_balance - sum_of_sales + sum_of_incoming_transactions - sum_of_outgoing_transactions
        self.save()

    def clean(self):
        super(Profile, self).clean()
        if self.sorting_order == Profile.ProfileType.FLATMATE and self.room_number is None:
            raise ValidationError("flatmates need a room number assigned")

    @property
    def total_amount_of_sales(self):
        return Sale.objects.filter(profile=self).count()

    @property
    def total_money_spent(self):
        return list(Sale.objects.filter(profile=self).aggregate(models.Sum('price')).values())[0]

    @property
    def d30_amount_of_sales(self):
        return Sale.objects.filter(profile=self, time__gte=timezone.now() - timezone.timedelta(days=30)).count()

    @property
    def d30_money_spent(self):
        return list(Sale.objects.filter(profile=self, time__gte=timezone.now() - timezone.timedelta(days=30)).aggregate(
            models.Sum('price')).values())[0]


class Price(models.Model):
    amount = models.FloatField(verbose_name="Price",
                               help_text="How much should products with this price assigned cost?")
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, blank=True)

    def __str__(self):
        return f"{self.name}: {self.amount:.2f} €"


class Product(models.Model):
    name = models.CharField(max_length=100)
    current_price = models.ForeignKey(Price, on_delete=models.RESTRICT)
    stock = models.IntegerField(default=0)
    active = models.BooleanField(default=True, help_text="Displays the item on the buy screen")
    dashboard_order = models.PositiveIntegerField(verbose_name="Sorting order", default=0,
                                                  help_text="Assign an sorting order to the drinks, "
                                                            "lower numbers appear at the top")
    chart_color = ColorField(default="#000000", verbose_name="Chart color", help_text="Color used to display in charts")
    image = models.ImageField()

    def __str__(self):
        return f"{self.name}"

    @property
    def get_current_price(self):
        return self.current_price.amount

    def get_sales(self):
        return Sale.objects.filter(product=self)

    def get_restocks(self):
        return InventoryCheck.objects.filter(product=self)

    def validate_stock(self):
        """
        recalculates the stock field, based on all sales, restocks and inventory countings
        :return:
        """

        all_sales = list(Sale.objects.filter(product=self).aggregate(models.Sum("amount")).values())[0]
        all_refills = \
            list(InventoryCheck.objects.filter(product=self).aggregate(models.Sum('correction_value')).values())[0]
        if not all_sales:
            all_sales = 0
        if not all_refills:
            all_refills = 0
        self.stock = all_refills - all_sales
        self.save()

    def got_counted(self):
        """
        returns qs containing all drinks that have been counted yet
        :return:
        """
        return len(InventoryCheck.objects.filter(product=self, check_type=InventoryCheck.CheckChoices.COUNTING)) > 0


class Sale(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    price = models.FloatField()

    amount = models.PositiveSmallIntegerField(default=1)
    time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        try:
            user = self.profile.get_username
        except (Profile.DoesNotExist, AttributeError):
            user = "[deleted]"

        try:
            product = self.product.name
        except (Product.DoesNotExist, AttributeError):
            product = "[deleted]"

        return f"{user} --> {product}"

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Sale, self).save(force_insert=force_insert, force_update=force_update, using=using,
                               update_fields=update_fields)
        self.profile.validate_balance()


class Transaction(models.Model):
    source = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, blank=True, related_name="source",
                               verbose_name="Source Account", help_text="Select Account to withdraw money from")
    target = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, blank=True, related_name="target",
                               verbose_name="Target Account", help_text="Select Account to credit the money to")
    amount = models.FloatField(help_text="How much money should be transferred?")
    time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        try:
            source = self.source.get_username
        except (Profile.DoesNotExist, AttributeError):
            source = "[null]"

        try:
            target = self.target.get_username
        except (Product.DoesNotExist, AttributeError):
            target = "[null]"

        return f"{source} --> {target} : {self.amount}"

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Transaction, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                      update_fields=update_fields)
        if self.source:
            self.source.validate_balance()

        if self.target:
            self.target.validate_balance()


class InventoryCheck(models.Model):
    class CheckChoices(models.IntegerChoices):
        RESTOCK = 1
        COUNTING = 2

    product = models.ForeignKey(Product, models.CASCADE)
    correction_value = models.IntegerField(verbose_name="Korrekturwert")
    timestamp = models.DateTimeField(auto_now_add=True)
    check_type = models.PositiveSmallIntegerField(choices=CheckChoices.choices)

    def __str__(self):
        return f"{self.product.name}: {self.get_check_type_display()}"
