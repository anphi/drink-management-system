function addValue(field_id, amount) {
    const currentVal = Number(document.getElementById(field_id).value);
    document.getElementById(field_id).value = currentVal + amount;
}

function setValue(field_id, value) {
    document.getElementById(field_id).value = Number(value);
}
