import base64

from django import template

register = template.Library()


@register.simple_tag
def png_base64(file):
    """
    returns an png image from file as a base64 string to embed it into the website
    :param file:
    :return:
    """
    try:
        with open(file.path, 'rb') as f:
            return f"data:image/png;base64,{base64.b64encode(f.read()).decode()}"
    except FileNotFoundError:
        return ""
