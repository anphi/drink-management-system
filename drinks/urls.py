from django.urls import path

from .views import views, drinks, user, prices

urlpatterns = [
    # base views
    path('', views.dashboard, name="dashboard"),
    path('history', views.history, name="history"),
    path('history/undo/<int:sale_id>', views.undo_sale, name="undo_sale"),

    # ordering logic
    path('order/<int:user_id>', views.order_user, name="order_user"),
    path('order/<int:user_id>/<int:product_id>', views.order_drink, name="order_full"),

    # user administration
    path('user/dashboard', user.user_dashboard, name="user_dashboard"),
    path('user/add', user.user_add, name="user_add"),
    path('user/<int:user_id>/edit', user.user_edit, name="user_edit"),
    path('user/pay', user.pay, name="user_pay"),
    path('user/transfer', user.transfer, name="user_transfer"),

    # drink administration
    path('drink/dashboard', drinks.drinks, name="drinks"),
    path('drink/add', drinks.drink_add, name='drink_add'),
    path('drink/<int:drink_id>/edit', drinks.drink_edit, name='drink_edit'),
    path('drink/<int:drink_id>/toggle', drinks.drink_toggle, name="drink_toggle"),
    path('drink/<int:drink_id>/changePrice', drinks.drink_change_price, name="drink_change_price"),
    path('drink/<int:drink_id>/restock', drinks.drink_restock, name="drink_restock"),
    path('drink/<int:drink_id>/count', drinks.drink_count, name="drink_count"),
    path('drink/recalculate', drinks.drink_recalculate_stock, name="drink_recalc_stock"),

    # price administration
    path('prices/dashboard', prices.prices_dashboard, name="prices_dashboard"),
    path('prices/add', prices.add, name="prices_add"),
    path('prices/<int:price_id>/edit', prices.edit, name="prices_edit"),
]
