from django.shortcuts import redirect, render

from drinks.forms import DrinkForm, ChangePriceForm, RestockForm
from drinks.models import Product, InventoryCheck


def drink_add(request):
    if request.method == 'POST':
        f = DrinkForm(request.POST, request.FILES)
        if f.is_valid():
            product = f.save(commit=False)
            if not f.cleaned_data['image']:
                product.image = "white.png"
            product.save()
            return redirect('drinks')
    else:
        f = DrinkForm()
    return render(request, 'drinks/drinkAdmin/drink_add.html', context={'f': f})


def drink_edit(request, drink_id):
    product = Product.objects.get(pk=drink_id)
    if request.method == 'POST':
        f = DrinkForm(request.POST, instance=product)
        if f.is_valid():
            f.save()
            return redirect('drinks')
    else:
        f = DrinkForm(instance=product)

    return render(request, 'drinks/drinkAdmin/drink_edit.html', {'f': f, 'product': product})


def drink_toggle(request, drink_id):
    product = Product.objects.get(pk=drink_id)
    product.active = not product.active
    product.save()
    return redirect('drinks')


def drink_change_price(request, drink_id):
    product = Product.objects.get(pk=drink_id)
    if request.method == 'POST':
        form = ChangePriceForm(request.POST, instance=product)
        if form.is_valid():
            form.save()
            return redirect('drinks')
    else:
        form = ChangePriceForm(instance=product)

    return render(request, 'drinks/drinkAdmin/drink_change_price.html', {'f': form, 'product': product})


def drink_restock(request, drink_id):
    product = Product.objects.get(pk=drink_id)
    if request.method == 'POST':
        f = RestockForm(request.POST)
        if f.is_valid():
            restock = InventoryCheck()
            restock.product = product
            restock.check_type = restock.CheckChoices.RESTOCK
            restock.correction_value = f.cleaned_data['bottles']
            restock.save()

            product.stock += f.cleaned_data['bottles']
            product.save()
            return redirect('drinks')
    else:
        f = RestockForm()
    return render(request, 'drinks/drinkAdmin/restock.html', context={'f': f, 'product': product})


def drink_count(request, drink_id):
    product = Product.objects.get(pk=drink_id)
    if request.method == 'POST':
        f = RestockForm(request.POST)
        if f.is_valid():
            restock = InventoryCheck()
            restock.product = product
            restock.check_type = restock.CheckChoices.COUNTING
            restock.correction_value = f.cleaned_data['bottles'] - product.stock
            restock.save()

            product.stock = f.cleaned_data['bottles']
            product.save()
            return redirect('drinks')
    else:
        f = RestockForm()
    return render(request, 'drinks/drinkAdmin/drink_count.html', context={'f': f, 'product': product})


def drinks(request):
    products = Product.objects.all().order_by('-active', 'name')  # - means reverse
    return render(request, 'drinks/drinkAdmin/drink_dashboard.html', context={'products': products})


def drink_recalculate_stock(request):
    for product in Product.objects.all():
        product.validate_stock()
    return redirect('drinks')
