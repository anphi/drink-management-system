from django.shortcuts import render, redirect

from drinks.forms import PriceForm
from drinks.models import Price


def add(request):
    if request.method == 'POST':
        f = PriceForm(request.POST)
        if f.is_valid():
            f.save()
            return redirect('prices_dashboard')
    else:
        f = PriceForm()

    return render(request, 'drinks/priceAdmin/price_add.html', context={'f': f})


def edit(request, price_id):
    price = Price.objects.get(pk=price_id)
    if request.method == 'POST':
        f = PriceForm(request.POST, instance=price)
        if f.is_valid():
            f.save()
            return redirect('prices_dashboard')
    else:
        f = PriceForm(instance=price)

    return render(request, 'drinks/priceAdmin/price_edit.html', context={'f': f, 'price': price})


def delete(request, price_id):
    price = Price.objects.get(pk=price_id)
    price.delete()
    return redirect('prices_dashboard')


def prices_dashboard(request):
    prices_obj = Price.objects.all()
    return render(request, 'drinks/priceAdmin/price_dashboard.html', context={'prices': prices_obj})
