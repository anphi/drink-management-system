from django.db import models
from django.shortcuts import redirect, render

from drinks.forms import UserEditForm, DebtForm, TransactionForm
from drinks.models import Profile


def user_add(request):
    if request.method == 'POST':
        f = UserEditForm(request.POST)
        if f.is_valid():
            f.save()
            return redirect('user_dashboard')
    else:
        f = UserEditForm()
    return render(request, 'drinks/userAdmin/user_add.html', context={'f': f})


def user_edit(request, user_id):
    user = Profile.objects.get(pk=user_id)
    if param := request.GET.get('delete'):
        if param == "true":
            user.delete()
            return redirect('user_dashboard')
    if request.method == 'POST':
        f = UserEditForm(request.POST, instance=user)
        if f.is_valid():
            f.save()
            return redirect('user_dashboard')
    f = UserEditForm(instance=user)

    return render(request, 'drinks/userAdmin/user_edit.html', context={'f': f, 'profile': user})


def user_dashboard(request):
    if request.GET.get('sort'):
        profiles = Profile.objects.exclude(username__exact="").order_by('room_number', 'username')
    else:
        profiles = Profile.objects.exclude(username__exact="").order_by('username')

    flatmates = [p for p in profiles if p.sorting_order == Profile.ProfileType.FLATMATE]
    guests = [p for p in profiles if p.sorting_order == Profile.ProfileType.GUEST]
    total_balances = Profile.objects.filter(active=True).aggregate(models.Sum("balance"))
    positive_balances = Profile.objects.filter(active=True, balance__gte=0).aggregate(models.Sum("balance"))
    negative_balances = Profile.objects.filter(active=True, balance__lt=0).aggregate(models.Sum("balance"))

    pos_user = Profile.objects.all().order_by("-balance").first()
    neg_user = Profile.objects.all().order_by("balance").first()

    context = {'flatmates': flatmates, 'guests': guests,
               'total_balances': list(total_balances.values())[0],
               'positive_balances': list(positive_balances.values())[0],
               'negative_balances': list(negative_balances.values())[0],
               'pos_user': pos_user,
               'neg_user': neg_user
               }
    return render(request, 'drinks/userAdmin/user_dashboard.html', context=context)


def pay(request):
    if request.method == 'GET':
        if request.GET.get("target"):
            target = Profile.objects.get(pk=request.GET.get("target"))
            f = DebtForm(initial={'target': target}, hide_source=True)
            return render(request, 'drinks/userAdmin/user_paydebt.html', context={'f': f, 'profile': target})
        else:
            f = DebtForm()
            f.fields['target'].queryset = Profile.objects.exclude(active=False).order_by("sorting_order", 'username')
            return render(request, 'drinks/userAdmin/user_paydebt.html', context={'f': f})

    if request.method == 'POST':
        f = DebtForm(False, request.POST)
        if f.is_valid():
            f.save()
            return redirect('dashboard')

        return render(request, 'drinks/userAdmin/user_paydebt.html', context={'f': f})


def transfer(request):
    if request.GET.get("source"):
        source = Profile.objects.get(pk=request.GET.get("source"))
        f = TransactionForm(initial={'source': source})
        f.fields['target'].queryset = Profile.objects.exclude(active=False).order_by("sorting_order", 'username')
    else:
        f = TransactionForm()

    if request.method == 'POST':
        f = TransactionForm(request.POST)
        if f.is_valid():
            f.save()
            return redirect('dashboard')

    return render(request, 'drinks/userAdmin/user_transaction.html', context={'f': f})
