from django.shortcuts import render, redirect

from drinks.models import Profile, Product, Sale


# Create your views here.

def dashboard(request):
    sorting_method = request.GET.get('sort', None)
    profiles = Profile.objects.exclude(username__exact="").filter(active=True)
    if sorting_method == "room":
        profiles = profiles.order_by("room_number")
    else:
        profiles = profiles.order_by("username")

    flatmates = [p for p in profiles if p.sorting_order == Profile.ProfileType.FLATMATE]
    guests = [p for p in profiles if p.sorting_order == Profile.ProfileType.GUEST]
    return render(request, 'drinks/dashboard.html',
                  context={'flatmates': flatmates, 'guests': guests})


def history(request):
    sales = Sale.objects.all().order_by('-time')[:15]  # get 15 latest sales
    return render(request, 'drinks/history.html', context={'sales': sales})


def undo_sale(request, sale_id):
    sale = Sale.objects.get(pk=sale_id)
    sale.product.stock += sale.amount
    sale.product.save()
    sale.delete()
    return redirect('history')


def order_user(request, user_id):
    try:
        user = Profile.objects.get(pk=user_id)
    except Profile.DoesNotExist:
        return redirect('dashboard')
    products = Product.objects.filter(active=True).order_by('dashboard_order', 'name')
    return render(request, 'drinks/ordering.html', context={"user": user, "products": products})


def order_drink(request, user_id, product_id):
    product = Product.objects.get(pk=product_id)
    profile = Profile.objects.get(pk=user_id)

    sale = Sale()
    sale.amount = 1
    sale.product = product
    sale.profile = profile
    sale.price = sale.product.current_price.amount * sale.amount
    sale.save()

    product.stock -= sale.amount
    product.save()
    # no need to change the profile balance since this is done in the save functions of the Sale class

    return render(request, 'drinks/sale_ok.html', context={'user_id': user_id, })
