import pandas
import plotly.graph_objects as go
from django.db import connection
from django.db import models
from django.http import JsonResponse
from django.utils import timezone

from drinks.models import Product, InventoryCheck, Profile

query_all_stock = "SELECT *, SUM(correction_value) OVER (PARTITION BY product_id ORDER BY timestamp) AS 'acc' FROM" \
                  "(SELECT timestamp, product_id, correction_value FROM drinks_inventorycheck UNION ALL SELECT " \
                  "time, product_id, -1 * amount FROM drinks_sale) ORDER BY product_id, timestamp;"

query_since_restock = "SELECT *, SUM(correction_value) OVER (PARTITION BY product_id ORDER BY timestamp) AS 'acc'" \
                      "FROM(" \
                      "         SELECT timestamp, product_id, correction_value" \
                      "         FROM drinks_inventorycheck" \
                      "        WHERE id IN (SELECT max(id) from drinks_inventorycheck GROUP BY product_id)" \
                      "         UNION ALL" \
                      "        SELECT time, product_id, -1 * amount" \
                      "        FROM drinks_sale" \
                      "        WHERE time >= (SELECT timestamp FROM drinks_inventorycheck WHERE " \
                      "drinks_inventorycheck.product_id == drinks_sale.product_id ORDER BY timestamp DESC LIMIT 1)" \
                      ")" \
                      "ORDER BY product_id, timestamp;"


def get_stock(since_last_restock=False):
    with connection.cursor() as cursor:
        if since_last_restock:
            cursor.execute(query_since_restock)
        else:
            cursor.execute(query_all_stock)
        row = cursor.fetchall()

    drink_names = {}
    df = pandas.DataFrame(columns=['time'])

    all_drinks = Product.objects.all().order_by('dashboard_order', 'name')
    for drink in all_drinks:
        drink_names[drink.pk] = drink.name
        df[drink.name] = None

    # list((timestamp, id, corr, acc))
    for el in row:
        df = df.append({'time': el[0], drink_names[el[1]]: el[3]}, ignore_index=True)
    df = df.set_index('time')
    df = df.resample("6H").max().ffill()
    df.fillna("", inplace=True)

    fig = go.Figure()
    fig.layout.yaxis.title = "count of drinks"
    fig.layout.title = "Stock of all drinks"
    for drink in all_drinks:
        fig.add_trace(go.Scatter(x=df.index, y=df[drink.name], name=drink.name, mode="lines+markers",
                                 line={"color": drink.chart_color, "shape": "spline", "smoothing": .8}))
    return fig.to_html(full_html=False, include_plotlyjs=False)


def get_loss():
    checks = InventoryCheck.objects.filter(check_type=InventoryCheck.CheckChoices.COUNTING) \
        .exclude(product__isnull=True).order_by("timestamp")
    if len(checks) == 0:
        return JsonResponse({"error": "no data"})

    drink_names = {}
    df = pandas.DataFrame(columns=['time'])

    for check in checks:
        drink_names[check.product.pk] = check.product.name
        try:
            df[check.product.name]
        except KeyError:
            df[check.product.name] = None

        df = df.append({'time': check.timestamp, check.product.name: check.correction_value}, ignore_index=True)

    df = df.set_index('time')
    df = df.resample("D").sum()
    df.fillna("", inplace=True)

    fig = go.Figure()
    fig.layout.title = "Loss of all drinks"
    fig.layout.yaxis.title = "lost drinks"
    checked_products = [p for p in Product.objects.all().order_by("dashboard_order", "name") if p.got_counted()]
    for drink in checked_products:
        fig.add_trace(go.Scatter(x=df.index, y=df[drink.name], name=drink.name, mode="lines+markers",
                                 line={"color": drink.chart_color, "shape": "spline", "smoothing": .8}))
    return fig.to_html(full_html=False, include_plotlyjs=False)


def get_drinks_of_user(user: Profile):
    """
    aggregates all drinks a user has bought in the last month
    :param user: user to analyze
    :return:
    """
    # select name, filter for current user and 30d time range and count sales for individual product
    user_sales_30d = Product.objects.all().values('pk', 'name'). \
        filter(sale__profile=user, sale__time__gte=timezone.now() - timezone.timedelta(days=30)). \
        annotate(sales=models.Count('sale')). \
        order_by('-sales', 'name')

    return user_sales_30d


def get_users_of_drink(drink: Product):
    """
    aggregates all users that have bought a drink in the last month
    :param drink: drink to analyze
    :return:
    """
    # select name, filter for current user and 30d time range and count sales for individual product
    drink_sales_30d = Profile.objects.all().values('pk', 'username'). \
        filter(sale__product=drink, sale__time__gte=timezone.now() - timezone.timedelta(days=30)). \
        annotate(sales=models.Count('sale')). \
        order_by('-sales', 'username')

    return drink_sales_30d
