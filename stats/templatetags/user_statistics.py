from django import template

from drinks.models import Profile, Product
from stats.statistics import get_drinks_of_user, get_users_of_drink

register = template.Library()


@register.inclusion_tag("stats/tags/drinks_of_user.html")
def drinks_of_user(user: Profile):
    products = get_drinks_of_user(user)
    display = len(products) > 0
    return {'products': products, 'user': user, 'display': display}


@register.inclusion_tag("stats/tags/users_of_drink.html")
def users_of_drink(drink: Product):
    users = get_users_of_drink(drink)
    display = len(users) > 0
    return {'users': users, 'drink': drink, 'display': display}
