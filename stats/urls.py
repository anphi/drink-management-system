from django.urls import path

from .views import views, data

app_name = "statistics"

urlpatterns = [
    path('drinks', views.stock, name="stock"),
    path('drinksofuser', views.drinks_of_user, name="drinks_of_user"),
    path('usersofdrink', views.users_of_drink, name="users_of_drink"),

    # data
    path('data/stock', data.stock, name="api_stock"),
    path('data/loss', data.loss, name="api_loss"),
]
