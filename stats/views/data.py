from django.shortcuts import render
from django.views.decorators.clickjacking import xframe_options_exempt

from stats.statistics import get_stock, get_loss


@xframe_options_exempt
def stock(request):
    return render(request, 'stats/stat_iframe.html', {'code': get_stock()})


@xframe_options_exempt
def loss(request):
    return render(request, 'stats/stat_iframe.html', {'code': get_loss()})
