SELECT *, SUM(correction_value) OVER (PARTITION BY product_id ORDER BY timestamp) AS 'acc'
FROM (
         SELECT timestamp, product_id, correction_value
         FROM drinks_inventorycheck
         UNION ALL
         SELECT time, product_id, -1 * amount
         FROM drinks_sale)
ORDER BY product_id, timestamp;