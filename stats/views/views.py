from django.shortcuts import render

from drinks.models import Profile, Product


def stock(request):
    return render(request, 'stats/stock.html')


def drinks_of_user(request):
    users = Profile.objects.filter(active=True).order_by('username')
    return render(request, 'stats/drinks_of_user.html', {'users': users})


def users_of_drink(request):
    products = Product.objects.filter(active=True).order_by('dashboard_order', 'name')
    return render(request, 'stats/users_of_drink.html', {'products': products})
